// Exponent Operator

// Template Literals
let getCube = Math.pow(2, 3);
console.log(`the cube of 2 is ${getCube}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
const [address1, address2, address3, address4] = address;
console.log(`i live at ${address1} ${address2}, ${address3} ${address4}`);


// Object Destructuring
const animal = {
  name: "Lolong",
  species: "saltwater crocodile",
  weight: "1075 kgs",
  measurement: "20 ft 3 in"
}
let { name, species, weight, measurement } = animal
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

let [number1, number2, number3, number4, number5] = numbers;

console.log(number1);
console.log(number2);
console.log(number3);
console.log(number4);
console.log(number5);

// let number1 = numbers[0];
// let number2 = numbers[1];
// let number3 = numbers[2];
// let number4 = numbers[3];
// console.log(numbers);

// Javascript Classes

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}
let dog1 = new Dog("Bugoy", "3", "Labrador");
let dog2 = new Dog("Pinat", "2", "Aspin");
console.log(dog1);
console.log(dog2);